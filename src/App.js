import './App.css';
import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Provider } from 'react-redux';
import {createStore } from 'redux';
import myReducers from './components/reducers';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import SignInUp from './components/SignInUp/SignInUp';
import MainApp from './components/MainApp/MainApp';

const store = createStore(myReducers);

class App extends Component {
  render(){
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path='/signin' component={SignInUp} />
            <Route path='/signup' component={SignInUp} />
            <Route path='/home' component={MainApp} />
            <Route path='/home' component={MainApp} />
            <Route path='/buy' component={MainApp} />
            <Route path='/sell' component={MainApp} />
            <Route path='/play' component={MainApp} />
            <Route path='/game/cardselection' component={MainApp} />
            <Route path='/game/matchmaking' component={MainApp} />
            <Route path='/' >
              <Redirect to="/home" />
            </Route>
          </Switch>
        </Router>
      </Provider>
    );
  }
}


        /*{store.getState().userReducer.current_user === undefined ? <SignInUp> : null}*/
       
export default App;
