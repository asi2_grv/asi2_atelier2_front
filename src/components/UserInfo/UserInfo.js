import React, { Component } from 'react';
import { Header, Icon, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { clearUser } from '../reducers/actions';

class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.user = this.props.user;
        this.disconnect = this.disconnect.bind(this);
    }

    disconnect(){
        this.props.dispatch(clearUser());
    }

    render() {
            return (
                <div>
                    <Button floated='right' onClick={this.disconnect}>Disconnect</Button>
                    <Header as='h3' floated='right'>
                        <Icon name="user circle outline" />
                        <Header.Content>
                            <span id="userNameId">{this.props.current_user.login}</span>
                            <Header.Subheader><span>{this.props.current_user.account.toFixed(2)}</span>$</Header.Subheader>
                        </Header.Content>
                    </Header>
                </div>
            );   
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    }
};

export default connect(mapStateToProps)(UserInfo);