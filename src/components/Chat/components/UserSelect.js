import { Component } from 'react';
import { connect } from 'react-redux';

import { Dropdown } from 'semantic-ui-react';

import { selectMessageUser, loadMessages } from '../../reducers/actions';

import chatREST from '../../network/ChatREST';

class UserSelect extends Component {
    constructor(props) {
        super(props);
        this.onSelectChatUser = this.onSelectChatUser.bind(this);
        this.handleGetHistory = this.handleGetHistory.bind(this);

        //TODO: select default user
        //this.props.dispatch(selectMessageUser(this.props.userList[0]))
    }
    onSelectChatUser(ev, { value: userId }) {
        this.props.dispatch(selectMessageUser(this.props.userList.find(user => user.id === userId)));
    }
    componentDidMount() {
        chatREST.getHistory(this.handleGetHistory, {
            sender: this.props.currentUser.id,
            receiver: this.props.currentMessageUser.id
        });
    }
    componentDidUpdate(prevProps) {
        if (!this.props.currentMessageUser || !this.props.currentUser) return;
        if (prevProps.currentMessageUser?.id !== this.props.currentMessageUser?.id || prevProps.currentUser?.id !== this.props.currentUser?.id)
            chatREST.getHistory(this.handleGetHistory, {
                sender: this.props.currentUser.id,
                receiver: this.props.currentMessageUser.id
            });
    }
    handleGetHistory(err, data) {
        this.props.dispatch(loadMessages(data));
    }
    render() {
        const dropDownOptions = [];
        for (const idx in this.props.userList) {
            const user = this.props.userList[idx];
            dropDownOptions.push({
                key: idx,
                value: user.id,
                icon: 'user circle',
                //TODO: can be used for custom user icons
                /*  
                    className: `user_${user.id}`, 
                    OR: image: { avatar: true, src: '/images/avatar/small/stevie.jpg' },
                */
                text: `${user.surName} ${user.lastName}`
            })
        }
        //TODO sort dropDownOptions ?
        return (
            <Dropdown fluid search selection
                placeholder="Select User"
                options={dropDownOptions}
                onChange={this.onSelectChatUser}
                value={this.props.currentMessageUser?.id}
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        userList: state.chatReducer.userList,
        currentMessageUser: state.chatReducer.currentMessageUser,
        currentUser: state.chatReducer.currentUser
    }
};

export default connect(mapStateToProps)(UserSelect);
