import { Component } from 'react';
import { connect } from 'react-redux';

import { Form, Button, Icon, TextArea } from 'semantic-ui-react';

import { addMessage } from '../../reducers/actions';

import chatSocket from '../../network/ChatSocket';

class Input extends Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.state = { chatInput: "" }
    }

    handleInput(ev, { value: content }) {
        if(content.slice(-1) === '\n') {
            //TODO: hotkey instead ?
            this.handleOnClick();
        }
        else {
            this.setState({ chatInput: content });
        }
    }
    handleOnClick() {
        chatSocket.sendMessage({
            sender: this.props.currentUser.id,
            receiver: this.props.currentMessageUser.id,
            content: this.state.chatInput
        })

        this.props.dispatch(addMessage({
            sender: this.props.currentUser.id,
            time: Math.floor(+new Date() / 1000),
            content: this.state.chatInput
        }));
        this.setState({ chatInput: "" });
    }

    render() {
        return (
            <Form>
                <Form.Field>
                    <TextArea rows={2} style={{ resize: "None" }} onChange={this.handleInput} value={this.state.chatInput}></TextArea>
                </Form.Field>
                <Button fluid icon labelPosition="right" onClick={this.handleOnClick}>
                    <Icon name="right arrow" />
                Send
            </Button>
            </Form>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentMessageUser: state.chatReducer.currentMessageUser,
        currentUser: state.chatReducer.currentUser
    }
};

export default connect(mapStateToProps)(Input);
