import { Component } from 'react';
import { connect } from 'react-redux';

import { animateScroll } from 'react-scroll';

import { Segment, Button, Icon } from 'semantic-ui-react';

import History from './components/History';
import UserSelect from './components/UserSelect';
import Input from './components/Input';
import ChatUserInfo from './components/ChatUserInfo';
import UserModel from '../models/User';

import { loadUsers, selectCurrentUser, selectMessageUser } from '../reducers/actions';

import chatSocket from '../network/ChatSocket';
import chatREST from '../network/ChatREST';


class Chat extends Component {
    constructor(props) {
        super(props);
        this.onGetUserList = this.onGetUserList.bind(this);
        this.onChatRetracted = this.onChatRetracted.bind(this);
        this.state = { retracted: true }

        //TODO: instead of getting all users from Spring, get connected user ids from GET {node}/chat/users
        // We can't do it now as we need to know the current user ID to get it to work, and we use the list to do that
        // It should be done with auth in production
        chatREST.getUsers(this.onGetUserList);
    }
    onGetUserList(err, data) {
        if (err) {
            console.log(err);
            return;
        }
        let availableUsers = [];
        console.log(data)
        for (const user of data) {
            availableUsers.push(new UserModel(user));
        }
        availableUsers = availableUsers.filter(user => user.id !== this.props.currentUser.id);



        this.props.dispatch(loadUsers(availableUsers));
        this.props.dispatch(selectMessageUser(availableUsers[0]));
        this.props.dispatch(selectCurrentUser(this.props.currentUser));

        chatSocket.setSenderId(this.props.currentUser.id);
    }
    onChatRetracted() {
        this.setState({ retracted: !this.state.retracted });
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.retracted !== this.state.retracted)
            this.scrollToBottom();
    }
    scrollToBottom(speed = 0) {
        animateScroll.scrollToBottom({
            containerId: "chat_autoscroll",
            duration: speed
        });
    }
    render() {
        if (this.state.retracted) {
            return (
                <Segment style={{ overflow: "auto", width: "25rem", position: "absolute", bottom: 0, right: "5rem", "z-index": "1000" }}>
                    <Button attached='bottom' onClick={this.onChatRetracted}>Chat</Button>
                </Segment>
            );
        }
        else {
            return (
                <Segment style={{ overflow: "auto", width: "25rem", position: "absolute", bottom: 0, right: "5rem", "z-index": "1000" }}>
                    <ChatUserInfo />
                    <Button icon onClick={this.onChatRetracted} style={{ position: "absolute", right: "1rem", top: "1rem" }}>
                        <Icon>
                            <Icon name="close"></Icon>
                        </Icon>
                    </Button>
                    <UserSelect />
                    <History />
                    <Input />
                </Segment>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentUser: state.userReducer.current_user
    }
};

export default connect(mapStateToProps)(Chat);
