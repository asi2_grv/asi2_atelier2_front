import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectuser } from '../reducers/actions';
import { createUser, updateUser, connectUser } from '../Fetch';

class SignInUp extends Component {
    constructor(props) {
        super(props);
        this.state = { form: {}, type: "SignUp" };
        this.handleChange = this.handleChange.bind(this);
        this.changeForm = this.changeForm.bind(this);
        this.process_data_creation = this.process_data_creation.bind(this);
        this.login = this.login.bind(this);
        this.addUser = this.addUser.bind(this);
        this.process_data_user = this.process_data_user.bind(this);
    }

    handleChange(ev) {
        const form = { ...this.state.form };
        form[ev.target.id] = ev.target.value;
        this.setState({ form });
    }

    process_data_creation(user) {
        connectUser(user).then((response) => {
            if (response > 0)//TODO: display error msg
                updateUser(this.process_data_user, response);
        });
    }

    process_data_user(data) {
        this.props.dispatch(selectuser(data));
        this.props.history.push('/home');
    }

    addUser() {
        const user = {
            "login": this.state.form.accountname,
            "pwd": this.state.form.password,
            "lastName": this.state.form.name,
            "surName": this.state.form.surname,
            "account": 2000
        }
        createUser(this.process_data_creation, user);
    }

    changeForm() {
        if (this.state.type === "SignUp") {
            this.setState({ type: "SignIn" });

        } else if (this.state.type === "SignIn") {
            this.setState({ type: "SignUp" });
        }
    }

    login() {
        const user = {
            "login": this.state.form.login,
            "pwd": this.state.form.password
        }
        connectUser(user).then((response) => {
            if (response > 0) //TODO: display error msg
                updateUser(this.process_data_user, response);
        });
    }

    render() {
        if (this.state.type === "SignUp") {
            return (<div>
                User Form
                <div>
                    <div>
                        Account name :
                    <input type="text" id="accountname" name="accountname" onChange={this.handleChange} required></input>
                    </div>
                    <div>
                        Name :
                    <input type="text" id="name" name="name" onChange={this.handleChange} required></input>
                    </div>
                    <div>
                        Surname :
                    <input type="text" id="surname" name="surname" onChange={this.handleChange} required></input>
                    </div>
                    <div>
                        Password :
                    <input type="password" id="password" name="password" onChange={this.handleChange} required></input>
                    </div>
                    <div>
                        Re tape password :
                    <input type="password" id="repassword" name="repassword" onChange={this.handleChange} required></input>
                    </div>
                    <button onClick={this.addUser}>+OK</button>
                    <button onClick={this.changeForm}>Sign In</button>
                </div>
            </div>);
        } else {
            return (<div>
                Login
                <div>
                    <div>
                        Login :
                    <input type="text" id="login" name="login" onChange={this.handleChange} required></input>
                    </div>
                    <div>
                        Password :
                    <input type="password" id="password" name="password" onChange={this.handleChange} required></input>
                    </div>
                    <button onClick={this.login}>Login</button>
                    <button onClick={this.changeForm}>Sign Up</button>
                </div>
            </div>);
        }
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        user_list: state.userReducer.user_list
    }
};

export default connect(mapStateToProps)(SignInUp);