import React, { Component } from 'react';
import { BrowserRouter as Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Grid } from 'semantic-ui-react';
import Market from '../Market/Market';
import Home from '../Home/Home';
import UserHeader from './components/UserHeader';
import RoomMenu from '../Game/RoomMenu/RoomMenu';
import CardSelection from '../Game/CardSelection/CardSelection';
import Matchmaking from '../Game/Matchmaking/Matchmaking';
import Chat from '../Chat/Chat'

class MainApp extends Component {
    render() {
        if (this.props.current_user === undefined) {
            return (<Redirect to="/signin" />)
        } else {
            return (
                <span>
                    <Grid.Row>
                        <UserHeader type={this.props.location.pathname.toLowerCase().slice(1)} />
                    </Grid.Row>
                    <Grid.Row>
                        <Chat />
                        <Switch>
                            <Route path='/home' component={(props) => <Home {...props} type="home" />} />
                            <Route path='/buy' component={(props) => <Market {...props} type="buy" />} />
                            <Route path='/sell' component={(props) => <Market {...props} type="sell" />} />
                            <Route path='/play' component={RoomMenu} />
                            <Route path='/game/cardselection' component={CardSelection} />
                            <Route path='/game/matchmaking' component={Matchmaking} />
                        </Switch>
                    </Grid.Row>

                </span>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    }
};

export default connect(mapStateToProps)(MainApp);
