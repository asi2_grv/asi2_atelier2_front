import React, { Component } from 'react';
import { Grid, Header, Table }from 'semantic-ui-react';
import CardList from './CardList';

class ListView extends Component {
    render() {
        let display_cards =
            <Grid>
                <Header as='h3'>My Card List</Header>
                <Table selectable celled id="cardListId">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Family</th>
                            <th>HP</th>
                            <th>Energy</th>
                            <th>Defence</th>
                            <th>Attack</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <CardList />
                    </tbody>
                </Table>
        </Grid>
        return(display_cards);
    }
}

export default ListView;