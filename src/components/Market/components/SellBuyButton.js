import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { selectuser, loadCardListToSell, clearCard } from '../../reducers/actions'
import { updateUser, updateCardToSell, buy_card, sell_card } from '../../Fetch'

class SellBuyButton extends Component {
    constructor(props) {
        super(props);
        this.Buy = this.Buy.bind(this);
        this.Sell = this.Sell.bind(this);
        this.process_data_user = this.process_data_user.bind(this);
        this.process_data_sell_buy = this.process_data_sell_buy.bind(this);
        this.process_data_card_list_to_sell = this.process_data_card_list_to_sell.bind(this);
    }

    process_data_card_list_to_sell(data){
        this.props.dispatch(loadCardListToSell(data))
    }

    process_data_user(data){
        this.props.dispatch(selectuser(data));
        updateCardToSell(this.process_data_card_list_to_sell);
    }

    process_data_sell_buy(){
        this.props.dispatch(clearCard());
        updateUser(this.process_data_user, this.props.current_user.id);
    }

    Buy(){
        let body = {
            "user_id": this.props.current_user.id,
            "card_id": this.props.current_card.id
        }
        buy_card(this.process_data_sell_buy, body);        
    }

    Sell(){
        let body = {
            "user_id": this.props.current_user.id,
            "card_id": this.props.current_card.id
        }
        sell_card(this.process_data_sell_buy, body);        
    }

    render() {
        let display;
        switch(this.props.current_view){
            case 'buy':
                return display=(
                    <Button bottom attached onClick={this.Buy}>
                        <Icon name="money" />
                        Buy at <span id="cardPriceId"> {this.props.price}$</span>
                    </Button>
                );
        
            case 'sell':
                return display=(
                    <Button bottom attached onClick={this.Sell}>
                        <Icon name="money" />
                        Sell at <span id="cardPriceId"> {this.props.price}$</span>
                    </Button>
                );

            default:
            return(display);
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_view: state.viewReducer.current_view,
        current_card: state.cardReducer.current_card,
        current_user: state.userReducer.current_user
    } };
  
export default connect(mapStateToProps)(SellBuyButton);