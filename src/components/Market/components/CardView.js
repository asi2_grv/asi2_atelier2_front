import React, { Component } from 'react';
import { Card, Grid, Image, Label, Form, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import SellBuyButton from './SellBuyButton';

class CardView extends Component {
    constructor(props) {
        super(props);
        this.card = this.props.card;
    }

    render() {
        if(this.props.current_card === undefined){
            return(<span></span>);
        }
        return(
            <Card special>
                <Card.Content>
                        <Grid>
                            {// TODO reprendre le code avec semantic react UI */
                            }
                            <div class="three column row">
                                <div class="column">
                                    <Icon name="heart outline "/><span id="cardHPId">{this.props.current_card.hp.toFixed(2)}</span> 
                                </div>
                                <div class="column">
                                        <h5>{this.props.current_card.name}</h5>
                                </div>
                                <div class="column">
                                    <span id="energyId">{this.props.current_card.energy}</span> <Icon name="lightning"/>
                                </div>
                            </div>
                        </Grid>
                </Card.Content>
                <div class="image imageCard">
                    <div class="blurring dimmable image">
                        <div class="ui inverted dimmer">
                            <div class="content">
                                <div class="center">
                                    <Button primary>Add Friend</Button>
                                </div>
                            </div>
                        </div>
                        <Image fluid>
                            <Label as='a' corner='left'>
                                {this.props.current_card.name}
                            </Label>
                            <Image centered id="cardImgId" src={this.props.current_card.imgUrl}/>
                        </Image>
                    </div>
                </div>
                <Card.Content>
                    <Form tiny>
                        <div class="field">
                            <label id="cardNameId"></label>
                            <textarea id="cardDescriptionId" class="overflowHiden" readonly="" rows="5" value={this.props.current_card.description}>
                            </textarea>
                        </div>
                    </Form>
                </Card.Content>
                <Card.Content>
                    <Icon name="heart outline "/><span id="cardHPId"> HP {this.props.current_card.hp.toFixed(2)}</span> 
                    <div class="right floated ">
                            <span id="cardEnergyId">Energy {this.props.current_card.energy}</span>
                        <Icon name="lightning"/>
                            
                    </div>
                </Card.Content>
                <Card.Content>
                    <span class="right floated">
                            <span id="cardAttackId"> Attack {this.props.current_card.attack.toFixed(2)}</span> 
                        <Icon name=" wizard"/>
                    </span>
                    <Icon name="protect"/>
                    <span id="cardDefenceId">Defense {this.props.current_card.defence.toFixed(2)}</span> 
                </Card.Content>
                <SellBuyButton 
                price = {this.props.current_card.price}
                />
            </Card>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        current_card: state.cardReducer.current_card,
        current_view: state.viewReducer.current_view
    } };
  
export default connect(mapStateToProps)(CardView);