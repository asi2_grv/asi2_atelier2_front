import React, { Component } from 'react';
import { Image } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { selectCard, loadCardListToSell } from '../../reducers/actions'
import { updateCardToSell } from '../../Fetch';


class CardList extends Component {
    constructor(props) {
        super(props);
        this.process_data = this.process_data.bind(this);
        this.availablecardtobuy();
    }

    process_data(data) {
        this.props.dispatch(loadCardListToSell(data))
    }

    availablecardtobuy() {
        updateCardToSell(this.process_data);
    }

    render() {
        if (this.props.current_user === undefined) {
            return (<h1><a href="/signin">Connect to see data</a></h1>);
        }
        let cardsContainer;
        if (this.props.current_view === 'sell')
            cardsContainer = this.props.current_user.cardList;
        else if (this.props.current_view === 'buy')
            cardsContainer = this.props.card_list_to_sell;
        if (cardsContainer.length === 0) {
            return (<h1>No card available</h1>);
        }
        else {
            return cardsContainer.map(
                (obj) =>
                    <tr onClick={() => { this.props.dispatch(selectCard(obj)) }}>
                        <td>
                            <Image avatar src={obj.smallImgUrl} /> <span>{obj.name}</span>
                        </td>
                        <td>{obj.description}</td>
                        <td>{obj.family}</td>
                        <td>{obj.hp.toFixed(2)}</td>
                        <td>{obj.energy}</td>
                        <td>{obj.defence.toFixed(2)}</td>
                        <td>{obj.attack.toFixed(2)}</td>
                        <td>{obj.price}$</td>
                    </tr>
            );
        }
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user,
        current_view: state.viewReducer.current_view,
        card_list_to_sell: state.cardReducer.card_list_to_sell
    }
};

export default connect(mapStateToProps)(CardList);