import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';

import CardView from './components/CardView';
import ListView from './components/List';

class Market extends Component {
    render() {
        let display_market = 
            <Grid>
                <Grid.Row columns={2}>
                    <Grid.Column width={10}>
                        <ListView />
                    </Grid.Column>
                    <Grid.Column width={6}>
                        <CardView />
                    </Grid.Column>
                </Grid.Row>
            </Grid>;
        return(display_market);
    }
}

export default Market;