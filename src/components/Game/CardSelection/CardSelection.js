import React, { useEffect, useState } from "react";
import { Grid, Button } from "semantic-ui-react";

import { socket } from "../../../config/socketio";
import CardList from "../components/CardList";

import { useDispatch, useSelector } from "react-redux";
import { connect } from 'react-redux';

import { useHistory } from "react-router";

import {
  addCardToDeck,
  removeCardFromDeck,
  copyUserCards,
  clearCardSelection
} from "../../reducers/cardSelection";

import{updateRoom}from "../../reducers/actions"
import SocketIoEvent from "../../../constants/socketIoEvent";

const MIN_CARD_SELECTION = 2;
const MAX_CARD_SELECTION = 4;

const CardSelection = () => {
  const [error, setError] = useState(false);

  const [message, setMessage] = useState("");

  const dispatch = useDispatch();
  const history = useHistory();

  const { userCard, cardSelected, current_user, current_room } = useSelector((state) => ({
    userCard: state.cardSelectionReducer.userCard,
    cardSelected: state.cardSelectionReducer.cardSelected,
    current_user: state.userReducer.current_user,
    current_room: state.roomReducer.current_room
  }));

  useEffect(() => {
    socket.on("player_select_card", (room) => {
      dispatch(updateRoom(room));
      history.push("/game/matchmaking");
    });

    socket.on("waiting", (data) => {
      setMessage(data);
    });
  },[dispatch, history]);

  useEffect(() => {
    // en arrivant dans le composant
    dispatch(clearCardSelection());
    dispatch(copyUserCards(current_user.cardList));
  }, [current_user.cardList, dispatch])

  const handleValidateSelection = () => {
    if (
      cardSelected.length > MAX_CARD_SELECTION ||
      cardSelected.length < MIN_CARD_SELECTION
    ) {
      setError(true);
      return;
    }
    let body = {
      cards: cardSelected,
      user: current_user,
      room: current_room
    }
    // create event
    socket.emit(SocketIoEvent.emit.CardSelection, body);
  };

  const handleSelectCard = (card) => {
    if (cardSelected.length >= MAX_CARD_SELECTION) return;
    dispatch(addCardToDeck(card.id));
  };

  const handleRemoveCard = (card) => {
    dispatch(removeCardFromDeck(card.id));
  };

  return (
    <>
      <Grid columns={2} relaxed="very" stackable>
        <Grid.Column verticalAlign="middle">
          <CardList onClick={handleSelectCard} cards={userCard} small ={false} button="select"/>
        </Grid.Column>
        <Grid.Column verticalAlign="middle">
          <CardList onClick={handleRemoveCard} cards={cardSelected} button="select"/>
        </Grid.Column>
      </Grid>
      {error &&
        `le nombre de carte doit etre compris entre ${MIN_CARD_SELECTION} et ${MAX_CARD_SELECTION} `}
      <Button onClick={handleValidateSelection}>Select Cards</Button>
      <span>{message}</span>
    </>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
      current_user: state.userReducer.current_user
  }
};

export default connect(mapStateToProps)(CardSelection);
