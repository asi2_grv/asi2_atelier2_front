import React from "react";
import CardView from "./CardView";
import { Card } from "semantic-ui-react";

const CardList = ({ onClick, cards, small, cardSelected, color, button }) => {
  return (
    <Card.Group centered>
      {cards?.map((card) => (
        <CardView
          key={card.id}
          onClick={() => onClick(card)}
          selected={cardSelected === card.id}
          card={card}
          small={small}
          color={color}
          button={button}
        />
      ))}
    </Card.Group>
  );
};

export default CardList;
