import React from "react";
import {
  Card,
  Grid,
  GridColumn,
  Image,
  Label,
  Form,
  Button,
  Icon,
} from "semantic-ui-react";
import { useSelector } from "react-redux";

const CardView = ({ card, selected, onClick, small, color, button }) => {

  const { current_card_to_play, current_user, current_player_turn } = useSelector((state) => ({
    current_card_to_play: state.matchmakingReducer.current_card_to_play,
    current_user: state.userReducer.current_user,
    current_player_turn: state.roomReducer.current_player_turn
  }));

  function selectionButton(button){
    switch(button){
      case 'select':
        return(<Button onClick={onClick}>Select Card</Button>);

      case 'choose':
        if(current_card_to_play === undefined && current_player_turn === current_user.id){
          return(<Button onClick={onClick}>Choose Card</Button>);
        }
        break;
        

      case 'attack':
        if(current_card_to_play !== undefined){
          return(<Button onClick={onClick}>Attack Card</Button>);
        }
        break;
        
      default:
        return ;
    }
  }

  return(
    <Card style={{ width: "150px" }} color={selected ? color : "grey"}>
      <Card.Content>
        <Grid>
          <div className="three column row">
            <GridColumn>
              <Icon name="heart outline" />
              <span id="cardHPId">{card.hp.toFixed(0)}</span>
            </GridColumn>
            <GridColumn>
              <h5>{card.name}</h5>
            </GridColumn>
            <GridColumn>
              <Icon name="lightning" />
              <span id="energyId">{card.energy.toFixed(0)}</span>{" "}
            </GridColumn>
          </div>
        </Grid>
      </Card.Content>
      <div className="image imageCard">
        <div className="blurring dimmable image">
          <Image fluid>
            <Label as="a" corner="left">
              {card.name}
            </Label>
            <Image centered id="cardImgId" src={card.imgUrl} />
          </Image>
        </div>
      </div>
      {!small && (
        <>
          <Card.Content>
            <Form>
              <div className="field">
                <label id="cardNameId"></label>
                <textarea
                  id="cardDescriptionId"
                  className="overflowHiden"
                  readOnly
                  rows="1"
                  value={card.description}
                ></textarea>
              </div>
            </Form>
          </Card.Content>
          <Card.Content>
            <Icon name="heart outline" />
            <span id="cardHPId"> HP {card.hp.toFixed(2)}</span>
            <div className="right floated ">
              <span id="cardEnergyId">Energy {card.energy.toFixed(2)}</span>
              <Icon name="lightning" />
            </div>
          </Card.Content>
        </>
      )}
      <Card.Content>
        <span className="right floated">
          <span id="cardAttackId"> Attack {card.attack.toFixed(2)}</span>
          <Icon name="wizard" />
        </span>
        <Icon name="protect" />
        <span id="cardDefenceId">Defense {card.defence.toFixed(2)}</span>
      </Card.Content>
      {selectionButton(button)}
      
    </Card>
  );
};

export default CardView;
