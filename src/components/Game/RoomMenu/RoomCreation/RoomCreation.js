import React, { Component } from 'react';
import { Button, Grid } from 'semantic-ui-react';
import { socket } from "../../../../config/socketio";
import { roomSelect } from '../../../reducers/actions/index'
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";

class RoomCreation extends Component {
    constructor(props) {
        super(props);
        
        this.creationRoom = this.creationRoom.bind(this);
        this.state = {form: {}, error_message: ""};
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        socket.on("sendRoomId", (room) => {
            this.props.dispatch(roomSelect(room));
            this.props.history.push("/game/cardselection");
        });
    }


    handleChange(ev) {
        const form = {...this.state.form};
        form[ev.target.id] = ev.target.value;
        this.setState({form});
    }

    creationRoom(){
        let body = {
            room: this.state.form,
            user: this.props.current_user
        }
        socket.emit("creationRoom", body);
    };

    render() {
        return (
            <Grid>
                <Grid.Row />
                <Grid.Row />
                <Grid.Row>
                    <Grid.Column width={2} />
                    <Grid.Column width={8}>
                        <div>Create room :</div>
                        <div>Room name : <input type="text" id="roomName" name="roomName" onChange={this.handleChange} required></input></div>
                        <div>Prix du parie : <input type="number" id="priceRoom" name="priceRoom" onChange={this.handleChange} required></input></div>
                        <Button onClick={this.creationRoom}>Create</Button>
                        <div>{this.state.error_message}</div>
                    </Grid.Column>
                    <Grid.Column width={2} />
                </Grid.Row>
            </Grid>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    }
};

export default withRouter(connect(mapStateToProps)(RoomCreation));