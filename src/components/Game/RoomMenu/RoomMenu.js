import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';

import RoomCreation from './RoomCreation/RoomCreation';
import RoomList from './RoomList/RoomList';

class RoomMenu extends Component {
    render() {
        return(
            <Grid>
                <Grid.Row columns={2} divided>
                    <Grid.Column>
                        <RoomCreation />
                    </Grid.Column>
                    <Grid.Column>
                        <RoomList />
                    </Grid.Column>
                </Grid.Row>
            </Grid>);
    }
}

export default RoomMenu;