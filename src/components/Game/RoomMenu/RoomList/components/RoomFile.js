import React, { Component } from 'react';
import { socket } from "../../../../../config/socketio";
import { connect } from 'react-redux';
import { Button }from 'semantic-ui-react';
import { withRouter } from "react-router-dom";


class RoomFile extends Component {
    constructor(props) {
        super(props);
        this.state = {rooms: [], errorMessage: ""};
        socket.emit("new_page", "");
    }

    componentDidMount() {
        socket.on("updaterooms", (rooms) => {
            this.setState({rooms: rooms});
        });

        socket.on("errorInRoom", (data) => {
            this.setState({errorMessage: data})
        });
    }

    joinRoom(obj){
        let body = {
            room: obj,
            user: this.props.current_user
        }
        console.log(body);
        socket.emit("adduser", body);
    };

    render() {
        let result = this.state.rooms.map(
            (obj) =>
                <tr key={obj.id}>
                    <td>{obj.roomName}</td>
                    <td>{obj.priceRoom}</td>
                    <td>{obj.numberPlayer}  {this.state.errorMessage}</td>
                    <td><Button onClick={() => this.joinRoom(obj)}>Join</Button></td>
                </tr>
        );
        return(result);
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_user: state.userReducer.current_user
    }
};

export default withRouter(connect(mapStateToProps)(RoomFile));