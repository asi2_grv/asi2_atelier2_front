import React, { Component } from 'react';
import { Grid, Header, Table }from 'semantic-ui-react';

import RoomFile from './components/RoomFile';

class RoomList extends Component {
    
    render() {
        return(
            <Grid>
                <Header as='h3'>Rooms List</Header>
                <Table selectable celled>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Number of players</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <RoomFile />
                    </tbody>
                </Table>
            </Grid>        
        );
    }
}

export default RoomList;