import React, { Component } from 'react';
import { Header } from 'semantic-ui-react';
import { connect } from 'react-redux';


class RoomInfo extends Component {
    render() {
        return(
        <span>
            <Header as='h2'>Room info :</Header>
            <div>Room name : {this.props.current_room?.roomName}</div>
            <div>Room price : {this.props.current_room?.priceRoom}$</div>

        </span>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        current_room: state.roomReducer.current_room
    }
};

export default connect(mapStateToProps)(RoomInfo);
