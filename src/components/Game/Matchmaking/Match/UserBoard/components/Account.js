import React, { useEffect, useState } from "react";
import { Header, Icon } from "semantic-ui-react";
import { useSelector } from "react-redux";
import { socket } from "../../../../../../config/socketio";

const Account = ({ login, surName, gotPowerPoint }) => {
  const { current_user } = useSelector((state) => ({
    current_user: state.userReducer.current_user
  }));

  const [powerPoint, setPowerPoint] = useState(3);

  useEffect(() => {
    socket.on("renderroom", (data) => {
      let user_index = data.users.findIndex(user => user.id === current_user.id);
      console.log(data)
      setPowerPoint(data.users[user_index].powerPoint);
    });
      
  }, [current_user.id])
  

  return (
    <Header as="h3" textAlign="center">
      <Icon name="user circle outline" />
      <Header.Content>
        <span id="userNameId">{login}</span>
        <Header.Subheader>
          <span>{surName}</span>
        </Header.Subheader>
        {gotPowerPoint ? <Header.Subheader> Point d action : {powerPoint}</Header.Subheader> : null}
      </Header.Content>
    </Header>
  );
};

export default Account;
