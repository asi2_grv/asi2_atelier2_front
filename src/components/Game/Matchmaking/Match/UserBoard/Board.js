import React from "react";
import Account from "./components/Account";

import CardList from "../../../components/CardList";

const Board = ({ cards, dataUser, isTop, onClick, cardSelected, color, button }) => {
  return (
    <span>
      {isTop && <Account login={dataUser?.login} surname={dataUser?.surname} gotPowerPoint={false} />}
      <CardList
        cards={cards}
        onClick={onClick}
        small={true}
        cardSelected={cardSelected}
        color={color}
        button={button}
      />
      {!isTop && <Account login={dataUser?.login} surname={dataUser?.surname} gotPowerPoint={true} />}
    </span>
  );
};

export default Board;
