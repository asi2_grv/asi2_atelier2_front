import React, { useEffect, useState } from "react";
import Board from "./UserBoard/Board";
import { Button } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { selectCardToPlay, selectCardToAttack, updateRoom, selectPlayerTurn, selectuser } from "../../../reducers/actions";
import { updateUser } from '../../../Fetch';
import { useHistory } from "react-router";

import {socket}  from "../../../../config/socketio"
import { clearCardSelection } from "../../../reducers/cardSelection";

const Match = () => {
  //const [userCardSelected, setUserCardSelected] = useState(-1);
  //const [opponentCardSelected, setOpponentCardSelected] = useState(-1);

  const dispatch = useDispatch();
  const history = useHistory();

  const [attackMessage, setAttackMessage] = useState("");


  const { current_user, current_card_to_play, current_card_to_attack, current_room, current_player_turn } = useSelector((state) => ({
    current_user: state.userReducer.current_user,
    current_card_to_play: state.matchmakingReducer.current_card_to_play,
    current_card_to_attack: state.matchmakingReducer.current_card_to_attack,
    current_room: state.roomReducer.current_room,
    current_player_turn: state.roomReducer.current_player_turn
  }));
  
  const { opponentInfo, opponentCards, userInfo, userCards } = useSelector(
    (state) => ({
      userInfo: state.roomReducer.current_room?.users.find(
        (user) => user.id === current_user.id
      ),
      userCards: state.roomReducer.current_room?.users.find(
        (user) => user.id === current_user.id
      ).cardList,
      opponentInfo: state.roomReducer.current_room?.users.find(
        (user) => user.id !== current_user.id
      ),
      opponentCards: state.roomReducer.current_room?.users.find(
        (user) => user.id !== current_user.id
      ).cardList,
    })
  );

  const handleClickOpponentCard = (card) => {
    dispatch(selectCardToAttack(card));
  };

  const handleClickUserCard = (card) => {
    dispatch(selectCardToPlay(card));
  };

  useEffect(() => { 
    function process_data(data){
      dispatch(selectuser(data));
      dispatch(selectPlayerTurn(undefined));
      dispatch(updateRoom(undefined));
      dispatch(clearCardSelection())
      history.push("/home");
    };

    socket.on("renderroom", (room) => {
      dispatch(updateRoom(room));
    });

    socket.on("firstplayer", (data) => {
      dispatch(selectPlayerTurn(data))
    });

    socket.on("updateplayerturn", (data) => {
      dispatch(selectPlayerTurn(data))
    });

    socket.on("attackMessage", (data) => {
      setAttackMessage(data);
    });

    socket.on("endgame", () => {
      updateUser(process_data, current_user.id)
    });
  }, [current_user.id, dispatch, history]);

  useEffect(() => { 
    if(current_card_to_play !== undefined && current_card_to_attack !== undefined){
      const body = {
        play_card: {id: current_user.id, play_card: current_card_to_play},
        attacked_card: current_card_to_attack,
        current_room: current_room
      }
      socket.emit("attack", body);
      dispatch(selectCardToAttack(undefined));
      dispatch(selectCardToPlay(undefined));
    }
    if(current_room?.users.find(user => user.id === current_user.id).powerPoint === 0){
      changePlayer();
    }
  });

  function changePlayer(){
    let body = {
      id : current_user.id,
      current_room: current_room
    }
    socket.emit("changeuser", body);
  }

  function actionMessage() {
    if(current_player_turn === current_user.id){
      return(
        <span>
          <span>C est à votre tour   </span>
          <Button onClick={ changePlayer }>Fin du tour</Button>
          <span>{attackMessage}</span>
        </span>);
    } else {
      return(<span>Votre adversaire est en train de jouer</span>);
    }
  }

  return (
    <span>
      <Board
        isTop={true}
        cards={opponentCards}
        dataUser={opponentInfo}
        onClick={handleClickOpponentCard}
        color="red"
        button="attack"
      />
      <Board
        isTop={false}
        cards={userCards}
        dataUser={userInfo}
        onClick={handleClickUserCard}
        color="green"
        button="choose"
      />
      {actionMessage()}
    </span>
  );
};

export default Match;
