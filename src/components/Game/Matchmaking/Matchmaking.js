import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import Match from './Match/Match';
import RoomInfo from './RoomInfo/RoomInfo'

class Matchmaking extends Component {
    render() {
        return(
        <Grid>
          <Grid.Row columns={2} divided>
            <Grid.Column width={12}>
              <Match />
            </Grid.Column>
            <Grid.Column width={4}>
              <RoomInfo />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        );
    }
}

export default Matchmaking;
