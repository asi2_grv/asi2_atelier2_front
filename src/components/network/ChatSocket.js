import openSocket from 'socket.io-client';

class ChatSocket {
    constructor() {
        this.socket = openSocket('http://localhost:8080');
    }
    subscribeToMessage(cb) {
        this.socket.on('getMessage', data => cb(null, data));
    }
    setSenderId(id) {
        this.socket.emit("setSenderId", { userId: id });
    }
    sendMessage(data) {
        this.socket.emit("sendMessage", data);
    }
}

export default new ChatSocket();
