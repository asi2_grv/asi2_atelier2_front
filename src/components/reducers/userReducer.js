const userReducer= (state={user_list:[], current_user:undefined},action) => {
    switch (action.type) {
        case 'SELECT_USER':
            return {...state, current_user:action.current_user};

        case 'CLEAR_USER':
            return {...state, current_user: action.current_user};
            
        default:
        return state;
    }
}

export default userReducer;