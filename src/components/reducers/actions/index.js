export const selectuser=(current_user)=>{
    return {type:'SELECT_USER',current_user:current_user};
}

export const selectCard=(current_card)=>{
    return {type:'SELECT_CARD',current_card:current_card};
}

export const selectView=(current_view)=>{
    return {type:'SELECT_VIEW',current_view:current_view};
}

export const loadCardListToSell=(card_list_to_sell)=>{
    return {type:'CARD_LIST_TO_SELL',card_list_to_sell:card_list_to_sell};
}

export const clearCard=()=>{
    return {type:'CLEAR_CARD',current_card:undefined};
}

export const clearUser=()=>{
    return{type:'CLEAR_USER', current_user:undefined};
}

export const roomSelect=(current_room)=>{
    return{type:'SELECT_ROOM', current_room:current_room};
}

export const updateRoom=(current_room)=>{
    return{type:'UPDATE_ROOM', current_room:current_room};
}

export const selectCardToPlay=(current_card_to_play)=>{
    return{type:'SELECT_CARD_TO_PLAY', current_card_to_play:current_card_to_play};
}

export const selectCardToAttack=(current_card_to_attack)=>{
    return{type:'SELECT_CARD_TO_ATTACK', current_card_to_attack:current_card_to_attack};
}

export const selectPlayerTurn=(current_player_turn)=>{
    return{type:'UPDATE_PLAYER_TURN', current_player_turn:current_player_turn};
}

//Chat actions 

export const loadMessages = (messageList) => {
    return { type: 'LOAD_MESSAGES', messageList };
}

export const addMessage = (message) => {
    return { type: 'ADD_MESSAGE', message };
}

export const loadUsers = (userList) => {
    return { type: 'LOAD_USERS', userList };
}
export const selectCurrentUser = (currentUser) => {
    return { type: 'SELECT_CURRENT_USER', currentUser };
}

export const selectMessageUser = (currentMessageUser) => {
    return { type: 'SELECT_MESSAGE_USER', currentMessageUser };
}
