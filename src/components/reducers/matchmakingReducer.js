const matchmakingReducer = (state = { current_card_to_play: undefined, current_card_to_attack: undefined }, action) => {
    switch (action.type) {
        case "SELECT_CARD_TO_PLAY":
            return { ...state, current_card_to_play: action.current_card_to_play };

        case "SELECT_CARD_TO_ATTACK":
            return { ...state, current_card_to_attack: action.current_card_to_attack}

        default:
            return state;
    }
};

export default matchmakingReducer;
