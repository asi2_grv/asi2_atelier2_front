import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardReducer from './cardReducer';
import viewReducer from './viewReducer';
import chatReducer from './chatReducer';
import {cardSelectionReducer} from "./cardSelection"
import roomReducer from './roomReducer';
import matchmakingReducer from './matchmakingReducer';

const globalReducer = combineReducers({
    userReducer: userReducer,
    cardReducer: cardReducer,
    viewReducer: viewReducer,
    cardSelectionReducer : cardSelectionReducer,
    roomReducer: roomReducer,
    matchmakingReducer: matchmakingReducer,
    chatReducer: chatReducer
 });
export default globalReducer;