import { ADD_CARD_TO_DECK, REMOVE_CARD_FROM_DECK,COPY_USER_CARD, CLEAR_CARD_SELECTION } from "./action-types.js";

const initialState = {
  userCard: [],
  cardSelected: [],
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_CARD_TO_DECK:
      return  {
        ...state,
        cardSelected: [
          ...state.cardSelected,
          state.userCard.filter((item) => item.id === action.cardId)[0],
        ],
        userCard: state.userCard.filter((item) => item.id !== action.cardId),
      };
    case REMOVE_CARD_FROM_DECK:
      return {
        ...state,
        userCard: [
          ...state.userCard,
          state.cardSelected.filter((item) => item.id === action.cardId)[0],
        ],
        cardSelected: state.cardSelected.filter(
          (item) => item.id !== action.cardId
        ),
      };
      case COPY_USER_CARD:
      return {
        ...state,
        userCard: action.cards,
      };
    case CLEAR_CARD_SELECTION:
      return { ...state, cardSelected: []};

    default:
      return state;
  }
};

export default reducer;
