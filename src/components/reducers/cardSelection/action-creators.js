import { ADD_CARD_TO_DECK ,REMOVE_CARD_FROM_DECK ,INIT,COPY_USER_CARD, CLEAR_CARD_SELECTION } from "./action-types";

export function init() {
  return {
    type: INIT,
  };
}

export function addCardToDeck(cardId) {
  return {
    type: ADD_CARD_TO_DECK,
    cardId,
  };
}

export function removeCardFromDeck(cardId) {
  return {
    type: REMOVE_CARD_FROM_DECK,
    cardId,
  };
}

export function copyUserCards(cards) {
  return {
    type: COPY_USER_CARD,
    cards,
  };
}

export function clearCardSelection() {
  return {
    type: CLEAR_CARD_SELECTION,
  };
}

