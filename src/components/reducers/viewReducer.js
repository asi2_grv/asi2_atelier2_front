const viewReducer= (state={current_view:undefined},action) => {
    switch (action.type) {
        case 'SELECT_VIEW':
            return {...state, current_view:action.current_view};

        default:
        return state;
    }
}

export default viewReducer;