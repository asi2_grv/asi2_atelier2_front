const roomReducer = (state = { current_room: undefined, current_player_turn : undefined }, action) => {
  switch (action.type) {
    case "SELECT_ROOM":
      return { ...state, current_room: action.current_room };
    case "UPDATE_ROOM":
      return { ...state, current_room: action.current_room };
    case "UPDATE_PLAYER_TURN":
      return { ...state, current_player_turn: action.current_player_turn };

    default:
      return state;
  }
};

export default roomReducer;
