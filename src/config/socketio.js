import io from 'socket.io-client'

global.CONFIG = require('../config.json');

//TODO use env or config.json variable instead
const socket = io(`http://localhost:1337`, {transports:['websocket']});
// const socket = io(`http://localhost:${CONFIG.port}`, {transports:['websocket']});

export {socket};