# Comparatif Framework Front-End :

| | ReactJS	| VueJS | Angular| 
| --- | --- | --- | --- |
| Vitual DOMS	| 1	| 1	| 0| 
| Dealing with complex cases	| 0	| 0	| 1| 
| Seo Friendly	| 1	| | 1| 
| Reusable components	| 1	| 1	| 1| 
| Proper Documentation	| 0	| 1	| 1| 
| Easy data flow	| 1	| | 1| 
| Syntax not a barrier 	| 0	| 0	| 1| 
| Easy Debugging & Testing	| 1	| 	| 0| 
| Easy to Learn	| 0	| 1	| 0| 
| Easy to maintain in large application	| 1	| 0	| 1| 
| cross platform supporter	| 1	| 1	| 1| 
| size of dependencies	| 0	| 1	| | 


# Qu’est ce que le CROSS ORIGIN ? En quoi cela est-il dangereux ?
 Il s'agit de la capacité de notre app web de renvoyer une ressource (requête) à une source différente que son origine.
 Cela peut amener à des failles de sécurité. On permet plus d'intéractions entre l'app et d'autres sites.
 ex : ne jamais faire ``Access-Control-Allow-Origin: *`` sur les HTTP Headers CORS

# Comment REACTJS fait-il pour afficher rapidement les modifications sur les composants ?
 - Virtual DOM & Real DOM
 Modifie la partie modifiée du DOM plutôt que l'entièreté (sur le Virtual). Puis écrase le DOM entier (sur le Real).
  
# Quelle est la fonction essentielle de REACTJS ?
 JSX : extension syntaxique du js. Produit des éléments React et les retranscrit dans le DOM.

#Quelle est la fonction essentielle de FLUX ?
 
# Qu’est ce que REDUX ?
 Redux est une bibliothèque permettant de gérer et de mettre à jour l'état des applications, en utilisant des événements appelés "actions" qui modifie des reducer contenu dans le store de l'application principale.

# Qu’est ce que JMS ? Est-ce spécifique à Springboot ?
 Java Message Service est une API Java pour les logiciels orientés messages. Elle fournit des modèles de messagerie génériques, elle peut être utilisés pour faciliter l'envoi et la réception de messages entre systèmes logiciels. 
 Ce n'est pas spécifique à SpringBoot est peut être utilisé dans tous systèmes Java.

# Quelles sont les différents modes de transmissions de JMS ?

## Émission

- Queue
- Topic

## Réception

- Synchrone
- Asynchrone

# Quel est le mode de transmission activé par défaut dans activeMq ?

Queue

# Qu’est ce que activeMq ?

 C'est un serveur de messagerie OpenSource. Permet d'échanger des messages entre vos applications web en utilisant STOMP sur des sockets web.

# Quels avantages proposent un bus de communication vis-à-vis de requêtes http classiques ?
 - La possibilité de faire des requêtes asynchrones
 - Permet un grand nombre de requêtes lourdes
 - Facilite la mise à l'échelle horizontale (scalabilité horizontale)

# Comment faire pour partager des modules Maven avec un partenaire extérieur ?

 Sur le module Maven : ```Maven install``` permet d'obtenir un jar.
 Déclarer le package du module dans le `pom.xml` du partenaire exterieur.
 Ensuite les modèles et leur foncitons associés sont disponible au projet.

# Comment faire pour exporter un composant REACTJS ?
 Export default `component`; // À la fin de la classe Component
# Quel est le pré-requis pour Springboot afin de pouvoir convertir automatiquement le message reçu dans un bus de communication en objet ?
 l'objet doit être serializable (implements Serializable)
# Comment est réalisée la traduction des messages reçus (bus de communication ou request http) en objet Java ? Quelles sont les prérequis ? Est-ce toujours possible ?
 Par Parsing avec les getter et setter.
 Pour le bus de communication ou requêtes http : implementer un parser ou dans le cas de Spring doit implémenter Serializable.
 ex : @RequestBody `objet` `nomdelobjet`
# Quelles sont les fonctionnalités des différentes annotations en Springboot ?:
- @EnableJpaRepositories // JPA = en rapport avec BD
 Permet de gérer les liens avec les BD
 Besoin d'ajouter une dependance JPA au pom.xml
- @EntityScan
  Indique à l'application où chercher les entities
- @ComponentScan
 Scanne le package et tous les sous-package à la recherche de components